package uz.pdp.appcompany.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.payload.WorkerAddDto;
import uz.pdp.appcompany.payload.WorkerDto;

import java.util.List;

@Service
public interface WorkerService {

    ResponseEntity<?> addWorker(WorkerAddDto workerAddDto);

    ResponseEntity<WorkerDto> getWorkerById(Long id);

    ResponseEntity<List<WorkerDto>> getWorkersList();

    ResponseEntity<?> updateWorker(Long id, WorkerAddDto workerAddDto);

    ResponseEntity<?> deleteWorker(Long id);

}
