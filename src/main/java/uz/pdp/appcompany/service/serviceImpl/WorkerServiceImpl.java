package uz.pdp.appcompany.service.serviceImpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.entity.Department;
import uz.pdp.appcompany.entity.Worker;
import uz.pdp.appcompany.mapper.WorkerMapper;
import uz.pdp.appcompany.payload.WorkerAddDto;
import uz.pdp.appcompany.payload.WorkerDto;
import uz.pdp.appcompany.repository.AddressRepo;
import uz.pdp.appcompany.repository.CompanyRepo;
import uz.pdp.appcompany.repository.DepartmentRepo;
import uz.pdp.appcompany.repository.WorkerRepo;
import uz.pdp.appcompany.service.WorkerService;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerServiceImpl implements WorkerService {

    private final DepartmentRepo departmentRepo;
    private final WorkerRepo workerRepo;
    private final AddressRepo addressRepo;
    private final WorkerMapper workerMapper;
    private final CompanyRepo companyRepo;

    public WorkerServiceImpl(DepartmentRepo departmentRepo, WorkerRepo workerRepo, AddressRepo addressRepo, WorkerMapper workerMapper, CompanyRepo companyRepo) {
        this.departmentRepo = departmentRepo;
        this.workerRepo = workerRepo;
        this.addressRepo = addressRepo;
        this.workerMapper = workerMapper;
        this.companyRepo = companyRepo;
    }

    @Override
    public ResponseEntity<?> addWorker(WorkerAddDto workerAddDto) {

        Long departmentId = workerAddDto.getDepartmentId();

        Optional<Department> optionalDepartment = departmentRepo.findById(departmentId);
        if (!optionalDepartment.isPresent())
            return new ResponseEntity<>("Worker department not found",
                    HttpStatus.NOT_FOUND);

        String phoneNumber = workerAddDto.getPhoneNumber();
        if (workerRepo.existsByPhoneNumber(phoneNumber))
            return new ResponseEntity<>("Worker with this phone number is already existent",
                    HttpStatus.FORBIDDEN);

        Worker worker = new Worker();
        worker.setName(workerAddDto.getName());
        worker.setPhoneNumber(phoneNumber);

        Integer homeNumber = workerAddDto.getHomeNumber();
        String street = workerAddDto.getStreet();

        Optional<Address> byHomeNumberAndAndStreet =
                addressRepo.findByHomeNumberAndStreet(homeNumber, street);

        if (byHomeNumberAndAndStreet.isPresent())
            worker.setAddress(byHomeNumberAndAndStreet.get());
        else{
            Address address = new Address();
            address.setStreet(street);
            address.setHomeNumber(homeNumber);
            address = addressRepo.save(address);
            worker.setAddress(address);
        }

        worker.setDepartment(optionalDepartment.get());

        worker = workerRepo.save(worker);

        return new ResponseEntity<>(workerMapper.toWorkerDto(worker), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<WorkerDto> getWorkerById(Long id) {
        Optional<Worker> byId = workerRepo.findById(id);
        return byId.map(worker -> ResponseEntity.ok(workerMapper.toWorkerDto(worker)))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<List<WorkerDto>> getWorkersList() {
        return ResponseEntity.ok(workerMapper.toWorkerDto(workerRepo.findAll()));
    }

    @Override
    public ResponseEntity<?> updateWorker(Long id, WorkerAddDto workerAddDto) {

        Optional<Worker> byId = workerRepo.findById(id);

        if (byId.isPresent()){

            Worker worker = byId.get();

            String phoneNumber = workerAddDto.getPhoneNumber();
            Long departmentId = workerAddDto.getDepartmentId();
            Integer homeNumber = workerAddDto.getHomeNumber();
            String street = workerAddDto.getStreet();

            if (!worker.getPhoneNumber().equals(phoneNumber)){
                if (workerRepo.existsByPhoneNumber(phoneNumber))
                    return new ResponseEntity<>("Worker with this phone number is already existent",
                        HttpStatus.FORBIDDEN);
                worker.setPhoneNumber(phoneNumber);
            }

            if (!worker.getDepartment().getId().equals(departmentId)){
                Optional<Department> optionalDepartment = departmentRepo.findById(departmentId);
                if (!optionalDepartment.isPresent())
                    return new ResponseEntity<>("Worker department not found",
                            HttpStatus.NOT_FOUND);
                worker.setDepartment(optionalDepartment.get());
            }

            Address address1 = worker.getAddress();

            if (!address1.getStreet().equals(street)
                || !address1.getHomeNumber().equals(homeNumber)){

                Optional<Address> byHomeNumberAndStreet =
                        addressRepo.findByHomeNumberAndStreet(homeNumber, street);

                if (byHomeNumberAndStreet.isPresent())
                    worker.setAddress(byHomeNumberAndStreet.get());
                else{

                    if (workerRepo.existsByAddressAndIdNot(address1, worker.getId()) ||
                        companyRepo.existsByAddress(address1)) {
                            Address address = new Address();
                            address.setHomeNumber(homeNumber);
                            address.setStreet(street);
                            address = addressRepo.save(address);
                            worker.setAddress(address);
                    }
                    else {
                        address1.setStreet(street);
                        address1.setHomeNumber(homeNumber);
                        addressRepo.save(address1);
                     }

                }

            }

            worker.setName(workerAddDto.getName());

            worker = workerRepo.save(worker);

            return new ResponseEntity<>(workerMapper.toWorkerDto(worker), HttpStatus.ACCEPTED);
        }

        return addWorker(workerAddDto);
    }

    @Override
    public ResponseEntity<?> deleteWorker(Long id) {

        Optional<Worker> byId = workerRepo.findById(id);
        if (!byId.isPresent())
            return new ResponseEntity<>("Worker with id = " + id + " not found",
                    HttpStatus.NOT_FOUND);

        Worker worker = byId.get();

        boolean b =
                workerRepo.existsByAddressAndIdNot(worker.getAddress(), worker.getId()) ||
                companyRepo.existsByAddress(worker.getAddress());

        workerRepo.delete(worker);

        if (!b)
            addressRepo.delete(worker.getAddress());

        return ResponseEntity.ok(workerMapper.toWorkerDto(worker));
    }

}