package uz.pdp.appcompany.service.serviceImpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.entity.Company;
import uz.pdp.appcompany.mapper.CompanyMapper;
import uz.pdp.appcompany.payload.CompanyAddDto;
import uz.pdp.appcompany.payload.CompanyDto;
import uz.pdp.appcompany.repository.AddressRepo;
import uz.pdp.appcompany.repository.CompanyRepo;
import uz.pdp.appcompany.repository.WorkerRepo;
import uz.pdp.appcompany.service.CompanyService;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {


    private final CompanyRepo companyRepo;
    private final CompanyMapper companyMapper;
    private final AddressRepo addressRepo;
    private final WorkerRepo workerRepo;


    public CompanyServiceImpl(CompanyRepo companyRepo, CompanyMapper companyMapper, AddressRepo addressRepo, WorkerRepo workerRepo) {
        this.companyRepo = companyRepo;
        this.companyMapper = companyMapper;
        this.addressRepo = addressRepo;
        this.workerRepo = workerRepo;
    }


    @Override
    public ResponseEntity<CompanyDto> getCompanyById(Long id) {
        Optional<Company> byId = companyRepo.findById(id);
        return byId.map(company -> ResponseEntity.status(HttpStatus.OK).body(
                companyMapper.toCompanyDto(company))).orElseGet(()
                -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

    @Override
    public ResponseEntity<List<CompanyDto>> getCompaniesList() {
        return ResponseEntity.status(200).body(companyMapper.toCompanyDto(companyRepo.findAll()));
    }

    @Override
    public ResponseEntity<?> addCompany(CompanyAddDto companyAddDto) {

        ResponseEntity<?> responseEntity = validAddress(companyAddDto);
        if (responseEntity != null)
            return responseEntity;

        Address address = new Address();
        address.setStreet(companyAddDto.getStreet());
        address.setHomeNumber(companyAddDto.getHomeNumber());
        address = addressRepo.save(address);

        Company company = companyMapper.toCompany(companyAddDto);
        company.setAddress(address);

        company = companyRepo.save(company);

        return new ResponseEntity<>(companyMapper.toCompanyDto(company), HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<?> updateCompany(Long id, CompanyAddDto companyAddDto) {

        Integer homeNumber = companyAddDto.getHomeNumber();
        String street = companyAddDto.getStreet();

        Optional<Company> byId = companyRepo.findById(id);

        if (byId.isPresent()) {

            Company company = byId.get();

            Address address = company.getAddress();

            if (!address.getStreet().equals(street) ||
                    !address.getHomeNumber().equals(homeNumber)) {

                if (addressRepo.existsAddressByHomeNumberAndStreet(homeNumber, street))
                    return new ResponseEntity<>("Updated Address belongs to other Company or" +
                            " a Company worker", HttpStatus.FORBIDDEN);

                address.setStreet(street);
                address.setHomeNumber(homeNumber);
                addressRepo.save(address);
            }

            company.setCorpName(companyAddDto.getCorpName());
            company.setDirectorName(companyAddDto.getDirectorName());

            company = companyRepo.save(company);

            return new ResponseEntity<>(companyMapper.toCompanyDto(company), HttpStatus.ACCEPTED);
        }

        return addCompany(companyAddDto);
    }

    @Override
    public ResponseEntity<?> deleteCompany(Long id) {

        Optional<Company> byId = companyRepo.findById(id);
        if (!byId.isPresent())
            return new ResponseEntity<>("Company with id = " + id + " not found",
                    HttpStatus.NOT_FOUND);

        Company company = byId.get();

        companyRepo.delete(company);
        addressRepo.delete(company.getAddress());

        return ResponseEntity.ok(companyMapper.toCompanyDto(byId.get()));
    }


    private ResponseEntity<?> validAddress(CompanyAddDto companyAddDto){

        Integer homeNumber = companyAddDto.getHomeNumber();
        String street = companyAddDto.getStreet();

        Optional<Company> byAddress_homeNumberAndAddress_street =
                companyRepo.findByAddress_HomeNumberAndAddress_Street(homeNumber, street);

        if (byAddress_homeNumberAndAddress_street.isPresent()){

            Company company = byAddress_homeNumberAndAddress_street.get();

            if (company.getCorpName().equals(companyAddDto.getCorpName())
                    && company.getDirectorName().equals(companyAddDto.getDirectorName())){

                return new ResponseEntity<>("This company in given address is already" +
                        " existent", HttpStatus.FORBIDDEN);
            }

            return new ResponseEntity<>("Given Address belongs to other Company",
                    HttpStatus.FORBIDDEN);
        }

        if (workerRepo.existsByAddress_HomeNumberAndAddress_Street(homeNumber, street))
            return new ResponseEntity<>("Given Address belongs to a Company worker", HttpStatus.FORBIDDEN);

        return null;
    }

}
