package uz.pdp.appcompany.service.serviceImpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.entity.Company;
import uz.pdp.appcompany.entity.Department;
import uz.pdp.appcompany.mapper.DepartmentMapper;
import uz.pdp.appcompany.payload.DepartmentAddDto;
import uz.pdp.appcompany.payload.DepartmentDto;
import uz.pdp.appcompany.repository.CompanyRepo;
import uz.pdp.appcompany.repository.DepartmentRepo;
import uz.pdp.appcompany.service.DepartmentService;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentServiceImpl implements DepartmentService {


    private final DepartmentRepo departmentRepo;
    private final CompanyRepo companyRepo;
    private final DepartmentMapper departmentMapper;


    public DepartmentServiceImpl(DepartmentRepo departmentRepo, CompanyRepo companyRepo, DepartmentMapper departmentMapper) {
        this.departmentRepo = departmentRepo;
        this.companyRepo = companyRepo;
        this.departmentMapper = departmentMapper;
    }


    @Override
    public ResponseEntity<DepartmentDto> getDepartmentById(Long id) {
        System.out.println("id = " + id);
        Optional<Department> byId = departmentRepo.findById(id);
        return byId.map(department -> ResponseEntity.ok(departmentMapper.toDepartmentDto(department))).orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }


    @Override
    public ResponseEntity<?> addDepartment(DepartmentAddDto departmentAddDto) {

        Long companyId;
        try {
            companyId = departmentAddDto.getCompanyId();
        }
        catch (HttpMessageNotReadableException e){
            return new ResponseEntity<>("Department Company can not be empty !", HttpStatus.NOT_ACCEPTABLE);
        }

        Optional<Company> byId = companyRepo.findById(companyId);
        if (!byId.isPresent())
            return new ResponseEntity<>("Department Company with id = "
                    + companyId + " not found", HttpStatus.NOT_FOUND);

        String name = departmentAddDto.getName();

        if (departmentRepo.existsByNameAndCompany_Id(name, companyId))
            return new ResponseEntity<>("This department in given company is already existent",
                    HttpStatus.FORBIDDEN);

        Department department = new Department();
        department.setName(name);
        department.setCompany(byId.get());

        department = departmentRepo.save(department);

        return new ResponseEntity<>(departmentMapper.toDepartmentDto(department), HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<List<DepartmentDto>> getDepartmentsList() {
        return ResponseEntity.ok(departmentMapper.toDepartmentDto(departmentRepo.findAll()));
    }


    @Override
    public ResponseEntity<?> updateDepartment(Long id, DepartmentAddDto departmentAddDto) {

        String name = departmentAddDto.getName();
        Long companyId = departmentAddDto.getCompanyId();

        Optional<Department> byId = departmentRepo.findById(id);
        if (byId.isPresent()) {

            Department department = byId.get();

            if (!department.getCompany().getId().equals(companyId)) {

                Optional<Company> byId1 = companyRepo.findById(companyId);
                if (!byId1.isPresent())
                    return new ResponseEntity<>("Department Company with id = " +
                            companyId + " not found", HttpStatus.NOT_FOUND);

                if (departmentRepo.existsByNameAndCompany(name, byId1.get()))
                    return new ResponseEntity<>("This department in given company is " +
                            "already existent", HttpStatus.FORBIDDEN);

                department.setCompany(byId1.get());
                department.setName(name);
                department = departmentRepo.save(department);
            }
            else if (!department.getName().equals(name)){

                if (departmentRepo.existsByNameAndCompany(name, department.getCompany()))
                    return new ResponseEntity<>("This department in given company is " +
                            "already existent", HttpStatus.FORBIDDEN);

                department.setName(name);
                department = departmentRepo.save(department);
            }

            return new ResponseEntity<>(departmentMapper.toDepartmentDto(department), HttpStatus.ACCEPTED);
        }

        return addDepartment(departmentAddDto);
    }


    @Override
    public ResponseEntity<?> deleteDepartment(Long id) {

        Optional<Department> byId = departmentRepo.findById(id);
        if (!byId.isPresent())
            return new ResponseEntity<>("Department with id = " + id + " not found",
                    HttpStatus.NOT_FOUND);

        Department department = byId.get();

        departmentRepo.delete(department);

        return ResponseEntity.ok(departmentMapper.toDepartmentDto(department));
    }

}
