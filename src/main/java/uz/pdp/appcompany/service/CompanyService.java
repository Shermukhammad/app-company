package uz.pdp.appcompany.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.payload.CompanyAddDto;
import uz.pdp.appcompany.payload.CompanyDto;

import java.util.List;

@Service
public interface CompanyService {

    ResponseEntity<CompanyDto> getCompanyById(Long id);

    ResponseEntity<List<CompanyDto>> getCompaniesList();

    ResponseEntity<?> addCompany(CompanyAddDto companyAddDto);

    ResponseEntity<?> updateCompany(Long id, CompanyAddDto companyAddDto);

    ResponseEntity<?> deleteCompany(Long id);

}
