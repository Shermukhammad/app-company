package uz.pdp.appcompany.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.payload.DepartmentAddDto;
import uz.pdp.appcompany.payload.DepartmentDto;

import java.util.List;

@Service
public interface DepartmentService {

    ResponseEntity<?> addDepartment(DepartmentAddDto departmentAddDto);

    ResponseEntity<DepartmentDto> getDepartmentById(Long id);

    ResponseEntity<List<DepartmentDto>> getDepartmentsList();

    ResponseEntity<?> updateDepartment(Long id, DepartmentAddDto departmentAddDto);

    ResponseEntity<?> deleteDepartment(Long id);

}
