package uz.pdp.appcompany.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.entity.Department;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkerDto implements Serializable {

    private String name;

    private String phoneNumber;

    private Address address;

    private Department department;

}
