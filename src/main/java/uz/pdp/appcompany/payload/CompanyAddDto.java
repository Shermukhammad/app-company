package uz.pdp.appcompany.payload;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appcompany.deserializer.CustomNumberDeserializer;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompanyAddDto implements Serializable {

    @NotBlank(message = "Company name not given")
    private String corpName;

    @NotBlank(message = "Director name not given")
    private String directorName;

    @NotBlank(message = "Street name not given")
    private String street;

    @NotNull(message = "Company home number not given !")
    @JsonDeserialize(using = CustomNumberDeserializer.class)
    @Min(value = 1, message = "Company home number is invalid !")
    private Integer homeNumber;

}
