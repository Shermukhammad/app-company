package uz.pdp.appcompany.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appcompany.entity.Address;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompanyDto implements Serializable {

    private String corpName;

    private String directorName;

    private Address address;

}
