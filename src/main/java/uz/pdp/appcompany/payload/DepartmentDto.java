package uz.pdp.appcompany.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appcompany.entity.Company;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DepartmentDto implements Serializable {

    private String name;

    private Company company;

}
