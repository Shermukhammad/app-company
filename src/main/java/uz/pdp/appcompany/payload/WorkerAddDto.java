package uz.pdp.appcompany.payload;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appcompany.deserializer.CustomNumberDeserializer;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkerAddDto implements Serializable {


    @NotBlank(message = "Worker name not given")
    private String name;


    @NotBlank(message = "Worker phone number not given")
    private String phoneNumber;


    @NotBlank(message = "Worker address street not given")
    private String street;


    @NotNull(message = "Worker address home number not given")
    @JsonDeserialize(using = CustomNumberDeserializer.class)
    @Min(value = 1, message = "Worker address home number is invalid !")
    private Integer homeNumber;


    @NotNull(message = "Worker department not given")
    @JsonDeserialize(using = CustomNumberDeserializer.class)
    @Min(value = 1, message = "Worker Department is invalid !")
    private Long departmentId;

}
