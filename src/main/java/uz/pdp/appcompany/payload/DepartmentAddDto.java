package uz.pdp.appcompany.payload;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appcompany.deserializer.CustomNumberDeserializer;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DepartmentAddDto implements Serializable {

    @NotBlank(message = "Department name not given")
    private String name;

    @NotNull(message = "Department Company not given !")
    @JsonDeserialize(using = CustomNumberDeserializer.class)
    @Min(value = 1, message = "Department Company is invalid !")
    private Long companyId;
}
