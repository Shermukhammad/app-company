package uz.pdp.appcompany.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class CustomNumberDeserializer extends JsonDeserializer<Long> {

    @Override
    public Long deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

        JsonNode jsonNode = jsonParser.readValueAsTree();
        String s = jsonNode.asText();

        s = s.trim();

        if (s.isEmpty())
            return -1L;

        try {
            return Long.valueOf(s);
        }
        catch (NumberFormatException e){
            return -1L;
        }

    }


}