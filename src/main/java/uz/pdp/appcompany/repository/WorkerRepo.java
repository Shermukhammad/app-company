package uz.pdp.appcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.entity.Worker;

public interface WorkerRepo extends JpaRepository<Worker, Long> {

    Boolean existsByPhoneNumber(String phoneNumber);

    Boolean existsByAddressAndIdNot(Address address, Long id);

    Boolean existsByAddress_HomeNumberAndAddress_Street(Integer homeNumber, String street);

}
