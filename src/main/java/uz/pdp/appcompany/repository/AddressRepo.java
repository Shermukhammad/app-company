package uz.pdp.appcompany.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcompany.entity.Address;

import java.util.Optional;

public interface AddressRepo extends JpaRepository<Address, Long> {

    Boolean existsAddressByHomeNumberAndStreet(Integer homeNumber, String street);

    Optional<Address> findByHomeNumberAndStreet(Integer homeNumber, String street);

}
