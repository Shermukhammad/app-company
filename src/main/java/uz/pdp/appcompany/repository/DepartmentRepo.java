package uz.pdp.appcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcompany.entity.Company;
import uz.pdp.appcompany.entity.Department;

public interface DepartmentRepo extends JpaRepository<Department, Long> {

    Boolean existsByNameAndCompany(String name, Company company);

    Boolean existsByNameAndCompany_Id(String name, Long companyId);

}
