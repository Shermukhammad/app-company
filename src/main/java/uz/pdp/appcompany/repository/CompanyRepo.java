package uz.pdp.appcompany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.entity.Company;

import java.util.Optional;

public interface CompanyRepo extends JpaRepository<Company, Long> {

    Optional<Company> findByAddress_HomeNumberAndAddress_Street(Integer homeNumber, String street);

    Boolean existsByAddress(Address address);
}
