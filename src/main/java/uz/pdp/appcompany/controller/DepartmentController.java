package uz.pdp.appcompany.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcompany.payload.DepartmentAddDto;
import uz.pdp.appcompany.payload.DepartmentDto;
import uz.pdp.appcompany.service.DepartmentService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
@RequestMapping("/api/department")
@Validated
public class DepartmentController {


    private final DepartmentService departmentService;


    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }


    @GetMapping("/get/{id}")
    public ResponseEntity<?> getDepartmentById(@PathVariable
                                                   @NotNull(message = "Id should not be null")
                                                   @Min(value = 1, message = "Id value is invalid")
                                                           Long id){
        return departmentService.getDepartmentById(id);
    }


    @GetMapping("/get/all")
    public ResponseEntity<List<DepartmentDto>> getDepartmentsList(){
        return departmentService.getDepartmentsList();
    }


    @PostMapping("/add")
    public ResponseEntity<?> addDepartment(@Valid @RequestBody DepartmentAddDto departmentAddDto){
            return departmentService.addDepartment(departmentAddDto);
    }


    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateDepartment(@PathVariable
                                                  @NotNull(message = "Id should not be null")
                                                  @Min(value = 1, message = "Id has to be positive integer")
                                                          Long id,
                                           @Valid @RequestBody DepartmentAddDto departmentAddDto){
        return departmentService.updateDepartment(id, departmentAddDto);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteDepartment(@PathVariable
                                                  @NotNull(message = "Id should not be null")
                                                  @Min(value = 1, message = "Id has to be positive integer")
                                                          Long id){
        return departmentService.deleteDepartment(id);
    }

}
