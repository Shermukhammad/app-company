package uz.pdp.appcompany.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcompany.payload.WorkerAddDto;
import uz.pdp.appcompany.payload.WorkerDto;
import uz.pdp.appcompany.service.WorkerService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/worker")
@Validated
public class WorkerController {


    private final WorkerService workerService;


    public WorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }


    @GetMapping("/get/{id}")
    public ResponseEntity<WorkerDto> getWorkerById(@PathVariable
                                                           @NotNull(message = "Id should not be null")
                                                           @Min(value = 1, message = "Id has to be positive integer")
                                                                   Long id){
        return workerService.getWorkerById(id);
    }


    @GetMapping("/get/all")
    public ResponseEntity<List<WorkerDto>> getWorkersList(){
        return workerService.getWorkersList();
    }


    @PostMapping("/add")
    public ResponseEntity<?> addWorker(@Valid @RequestBody WorkerAddDto workerAddDto){
        return workerService.addWorker(workerAddDto);
    }


    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateWorker(@PathVariable
                                              @NotNull(message = "Id should not be null")
                                              @Min(value = 1, message = "Id has to be positive integer")
                                                      Long id,
                                              @Valid @RequestBody WorkerAddDto workerAddDto){
        return workerService.updateWorker(id, workerAddDto);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteWorker(@PathVariable
                                              @NotNull(message = "Id should not be null")
                                              @Min(value = 1, message = "Id has to be positive integer")
                                                      Long id){
        return workerService.deleteWorker(id);
    }

}
