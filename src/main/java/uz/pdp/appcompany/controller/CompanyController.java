package uz.pdp.appcompany.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcompany.payload.CompanyAddDto;
import uz.pdp.appcompany.payload.CompanyDto;
import uz.pdp.appcompany.service.CompanyService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/company")
@Validated
public class CompanyController {


    private final CompanyService companyService;


    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }


    @GetMapping("/get/{id}")
    public ResponseEntity<CompanyDto> getCompanyById(@PathVariable
                                                         @NotNull(message = "Id should not be null")
                                                         @Min(value = 1, message = "Id has to be positive integer")
                                                                 Long id){
        return companyService.getCompanyById(id);
    }


    @GetMapping("/get/all")
    public ResponseEntity<List<CompanyDto>> getCompaniesList(){
        return companyService.getCompaniesList();
    }


    @PostMapping("/add")
    public ResponseEntity<?> addCompany(@Valid @RequestBody CompanyAddDto companyAddDto){
        return companyService.addCompany(companyAddDto);
    }


    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateCompany(@PathVariable
                                               @NotNull(message = "Id should not be null")
                                               @Min(value = 1, message = "Id has to be positive integer")
                                                       Long id,
                                           @Valid @RequestBody CompanyAddDto companyAddDto){
        return companyService.updateCompany(id, companyAddDto);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCompany(@PathVariable
                                               @NotNull(message = "Id should not be null")
                                               @Min(value = 1, message = "Id has to be positive integer")
                                                       Long id){
        return companyService.deleteCompany(id);
    }

}
