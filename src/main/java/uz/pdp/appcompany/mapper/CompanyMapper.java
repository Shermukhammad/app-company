package uz.pdp.appcompany.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.pdp.appcompany.entity.Company;
import uz.pdp.appcompany.payload.CompanyAddDto;
import uz.pdp.appcompany.payload.CompanyDto;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CompanyMapper {

    CompanyDto toCompanyDto(Company company);

    List<CompanyDto> toCompanyDto(List<Company> companies);

    Company toCompany(CompanyAddDto companyAddDto);

}
