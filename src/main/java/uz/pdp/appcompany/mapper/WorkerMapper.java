package uz.pdp.appcompany.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.pdp.appcompany.entity.Worker;
import uz.pdp.appcompany.payload.WorkerDto;

import java.util.List;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WorkerMapper {

    WorkerDto toWorkerDto(Worker worker);

    List<WorkerDto> toWorkerDto(List<Worker> workers);

}
